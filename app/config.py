# coding=utf-8
"""
Файлы конфигурации
"""
import os


DEBUG = True
ENCODING = 'utf8'
BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
DATABASE_FILE = os.path.join(BASE_DIR, 'database.db')
THREADS_PER_PAGE = 2
CSRF_ENABLED = True
CSRF_SESSION_KEY = "49WbQxTD359C12I68CLfiqm08NVJ4N4l"
SECRET_KEY = "lT80cCJhl7m3668bp2uj7N5173rZdD7e"