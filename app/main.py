# coding=utf-8
"""
Документация
"""
from app import config
from flask import Flask

app = Flask(__name__)
app.config.from_object(config)