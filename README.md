# Install

pyvenv часто поврежден в ubuntu, mint и debian. Ниже скрипт ручной установки.

    pyvenv-3.4 --without-pip myvenv
    source ./myvenv/bin/activate
    wget https://pypi.python.org/packages/source/s/setuptools/setuptools-3.4.4.tar.gz
    tar -vzxf setuptools-3.4.4.tar.gz
    cd setuptools-3.4.4
    python setup.py install
    cd ..
    wget https://pypi.python.org/packages/source/p/pip/pip-1.5.6.tar.gz
    tar -vzxf pip-1.5.6.tar.gz
    cd pip-1.5.6
    python setup.py install
    cd ..
    deactivate
    source ./myvenv/bin/activate

Далее потребуются некоторые дополнителньые пакеты:

    pip install beautifulsoup4
    pip install flask
    pip install Flask-Script
