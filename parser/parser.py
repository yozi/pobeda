# coding=utf-8
"""
Парсеры
"""
from copy import copy
import os
from time import sleep
import urllib.request
import urllib.parse
import sys
import datetime
from bs4 import BeautifulSoup

if __name__ == "__main__":
    sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from app.config import BASE_DIR


search_form_fields = {
    '__EVENTTARGET': '',
    '__EVENTARGUMENT': '',
    'eventArgument': '',
    'AvailabilitySearchInputFrameSearchView$RadioButtonMarketStructure': 'OneWay',
    'AvailabilitySearchInputFrameSearchView$TextBoxMarketOrigin1': 'Москва (VKO)',
    'AvailabilitySearchInputFrameSearchView$TextBoxMarketDestination1': 'Москва (VKO)',
    'AvailabilitySearchInputFrameSearchView$DropDownListMarketDay1': '1',
    'AvailabilitySearchInputFrameSearchView$DropDownListMarketMonth1': '2015-01',
    'AvailabilitySearchInputFrameSearchView$TextBoxMarketOrigin2': '',
    'AvailabilitySearchInputFrameSearchView$TextBoxMarketDestination2': '',
    'AvailabilitySearchInputFrameSearchView$DropDownListMarketDay2': '8',
    'AvailabilitySearchInputFrameSearchView$DropDownListMarketMonth2': '2015-01',
    'AvailabilitySearchInputFrameSearchView$DropDownListPassengerType_ADT': '1',
    'AvailabilitySearchInputFrameSearchView$DropDownListPassengerType_CHD': '0',
    'AvailabilitySearchInputFrameSearchView$DropDownListPassengerType_INFANT': '0',
    'AvailabilitySearchInputFrameSearchView$DropDownListSearchBy': 'scheduleSearch',
    'AvailabilitySearchInputFrameSearchView$ButtonSubmit': 'Найти рейсы',
}

destinations = (
    'Белгород (EGO)', 'Волгоград (VOG)', 'Екатеринбург (SVX)', 'Пермь (PEE)', 'Самара (KUF)', 'Сургут (SGC)',
    'Тюмень (TJM)')


class PobedaParser(object):
    """
    Парсер сайта Победа
    """
    base_url = 'https://booking.pobeda.aero'
    form_action = 'https://booking.pobeda.aero/FrameSearch.aspx'
    _security_key = None

    def get_security_key(self):
        if not self._security_key:
            request = urllib.request.Request(url=self.form_action)
            response = urllib.request.urlopen(request)
            security_key = response.url.split('/')[3]
            self._security_key = security_key

        return self._security_key

    def search_form_generator(self):
        """
        Генерация форм для поиска
        """
        for destination in destinations:
            for month in (1, 2, 3):
                for day in (1, 8, 15, 22):
                    form = copy(search_form_fields)
                    form['AvailabilitySearchInputFrameSearchView$TextBoxMarketDestination1'] = destination
                    form['AvailabilitySearchInputFrameSearchView$DropDownListMarketMonth1'] = '2015-{month}'.format(
                        month=month)
                    form['AvailabilitySearchInputFrameSearchView$DropDownListMarketMonth2'] = '2015-{month}'.format(
                        month=month)
                    form['AvailabilitySearchInputFrameSearchView$DropDownListMarketDay1'] = day
                    form['AvailabilitySearchInputFrameSearchView$DropDownListMarketDay2'] = day + 3
                    yield form

    def load_price_page(self, form):
        """
        Поиск странички с ценами
        """
        security_key = self.get_security_key()
        search_url = '/'.join([self.base_url, security_key, 'FrameSearch.aspx'])
        data = urllib.parse.urlencode(form).encode('utf-8')
        request = urllib.request.Request(url=search_url, data=data, method='POST')
        response = urllib.request.urlopen(request)
        sleep(2)  # попробуем не попасть под блокировку

        return BeautifulSoup(response.read())

    def load_data(self):
        forms = self.search_form_generator()
        time_chunk = str(datetime.datetime.now())
        for form in forms:
            price_page = self.load_price_page(form)
            save_path = os.path.join(BASE_DIR, 'data',
                                     form['AvailabilitySearchInputFrameSearchView$TextBoxMarketDestination1'],
                                     time_chunk)
            if not os.path.exists(save_path):
                os.makedirs(save_path)
            file_name = '{destination}_{month}_{day}.html'.format(
                destination=form['AvailabilitySearchInputFrameSearchView$TextBoxMarketDestination1'],
                month=form['AvailabilitySearchInputFrameSearchView$DropDownListMarketMonth1'],
                day=form['AvailabilitySearchInputFrameSearchView$DropDownListMarketDay1'],
            )
            save_file = os.path.join(save_path, file_name)
            with open(save_file, 'w') as f:
                f.write(str(price_page))


if __name__ == "__main__":
    parser = PobedaParser()
    parser.load_data()
    pass