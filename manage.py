# coding=utf-8
"""
Консольный менеджер приложения
"""

from flask.ext.script import Manager

from app.main import app

manager = Manager(app)


@manager.command
def hello():
    print("hello")


@manager.command
def parse_avia():
    print("parse_avia")

if __name__ == "__main__":
    manager.run()